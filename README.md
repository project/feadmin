CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

Front-End Administration module provides a way to administer Drupal core
concepts from any page on front via Drag&Drop and AJAX operations.

In particular, it supports via sub-modules:
* administer blocks
* administer menu items

REQUIREMENTS
------------

Front-End administration does not have any requirement modules in itself. Its
sub-module does depend on the Core module it administers.

RECOMMENDED MODULES
-------------------

Front-End administration is recommended with Toolbar, but this is not mandatory.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

* Activate the module
* Activate any sub-modules as per your needs
* Activate the enabled tools via admin configuration page:
    admin/config/system/front_end_administration
* Give proper permissions to your users
* Use the module on any front page.

TROUBLESHOOTING
---------------


FAQ
---


MAINTAINERS
-----------

Current maintainers:
 * Dom. - https://www.drupal.org/u/dom
    Sponsored by ACINO: www.acino.fr
